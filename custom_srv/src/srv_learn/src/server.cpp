#include "rclcpp/rclcpp.hpp"
#include "example_interfaces/srv/add_two_ints.hpp"

#include <memory>

void add(const std::shared_ptr<example_interfaces::srv::AddTwoInts::Request> request,
          std::shared_ptr<example_interfaces::srv::AddTwoInts::Response>      response)
{
  response->sum = request->a + request->b;
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Incoming request\na: %ld" " b: %ld",
                request->a, request->b);
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "sending back response: [%ld]", (long int)response->sum);
}

int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);

  std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("test_node");
  
  node->declare_parameter<std::string>("test_string", "xiaoming");
  node->declare_parameter<bool>("test_bool", true);
  node->declare_parameter<double>("test_double", 11.13);
  node->declare_parameter<int64_t>("test_int", 999);
  rclcpp::Service<example_interfaces::srv::AddTwoInts>::SharedPtr service =
  node->create_service<example_interfaces::srv::AddTwoInts>("add_two_ints", &add);
  
  rclcpp::spin(node);
  rclcpp::shutdown();
}